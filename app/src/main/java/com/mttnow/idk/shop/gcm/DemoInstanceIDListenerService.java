package com.mttnow.idk.shop.gcm;

import com.google.android.gms.iid.InstanceIDListenerService;
import com.mttnow.android.messageinbox.InboxOperations;
import com.mttnow.android.messageinbox.InboxTemplate;
import com.mttnow.identity.auth.client.Authentication;
import com.mttnow.idk.shop.login.SharedPreferencesAuthenticationProvider;

public class DemoInstanceIDListenerService extends InstanceIDListenerService {

  private InboxOperations inboxOperations;
  private SharedPreferencesAuthenticationProvider authenticationProvider;

  @Override
  public void onCreate() {
    super.onCreate();
    inboxOperations = new InboxTemplate(this);
    authenticationProvider = new SharedPreferencesAuthenticationProvider(this);
  }

  @Override
  public void onTokenRefresh() {
    super.onTokenRefresh();
    Authentication authentication = authenticationProvider.provideAuthentication();
    if (authentication != null) {
      authenticationProvider.provideAuthentication();
      inboxOperations.registerGCMInbox(authentication.getUserUuid(), this, null);
    }
  }
}
