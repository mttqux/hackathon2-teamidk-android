package com.mttnow.idk.shop.login;

import android.content.Context;

import com.mttnow.identity.auth.client.Authentication;
import com.mttnow.identity.auth.client.AuthenticationProvider;
import com.mttnow.identity.auth.client.exceptions.IdentityAuthenticationException;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class SharedPreferencesAuthenticationProvider implements AuthenticationProvider {

  private static final String UUID_KEY = "USER_UUID";
  private static final String TOKEN_KEY = "TOKEN";
  private static final String CREATED_KEY = "CREATED_AT";
  private static final String EXPIRES_KEY = "EXPIRES_IN";
  private static final String REFRESH_TOKEN_KEY = "REFRESH_TOKEN";

  private SecuredPreferences securePrefs;
  private Authentication authentication;

  public SharedPreferencesAuthenticationProvider(Context context) {
    securePrefs = new SecuredPreferences(context);
  }

  @Override
  public Authentication provideAuthentication() throws IdentityAuthenticationException {
    if (authentication != null) {
      return this.authentication;
    }
    this.authentication = getSharedPrefsAuthentication();
    return this.authentication;
  }

  @Override
  public void saveAuthentication(Authentication authentication) throws IdentityAuthenticationException {
    this.authentication = authentication;

    SecuredPreferences.Editor editor = securePrefs.edit();

    editor.putString(UUID_KEY, authentication.getUserUuid());
    editor.putString(TOKEN_KEY, authentication.getToken());
    editor.putString(CREATED_KEY, authentication.getCreatedAt().toString());
    editor.putString(EXPIRES_KEY, authentication.getExpiresAt().toString());
    editor.putString(REFRESH_TOKEN_KEY, authentication.getRefreshToken());
    editor.apply();
  }

  @Override
  public void clearAuthentication() throws IdentityAuthenticationException {
    this.authentication = null;
    SecuredPreferences.Editor editor = securePrefs.edit();
    editor.remove(UUID_KEY);
    editor.remove(TOKEN_KEY);
    editor.remove(CREATED_KEY);
    editor.remove(EXPIRES_KEY);
    editor.remove(REFRESH_TOKEN_KEY);
    editor.apply();
  }

  private Authentication getSharedPrefsAuthentication() {
    DateTimeFormatter dtf = ISODateTimeFormat.dateTime();

    String uuid = securePrefs.getString(UUID_KEY, null);
    String token = securePrefs.getString(TOKEN_KEY, null);
    String created = securePrefs.getString(CREATED_KEY, null);
    String expires = securePrefs.getString(EXPIRES_KEY, null);
    String refreshToken = securePrefs.getString(REFRESH_TOKEN_KEY, null);

    Authentication auth = new Authentication();
    auth.setUserUuid(uuid);
    auth.setToken(token);
    auth.setCreatedAt(created == null ? null : dtf.parseDateTime(created));
    auth.setExpiresAt(expires == null ? null : dtf.parseDateTime(expires));
    auth.setRefreshToken(refreshToken);

    return auth;
  }
}