package com.mttnow.idk.shop.login;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.mttnow.idk.shop.R;


public class LoginDialogFragment extends DialogFragment implements View.OnClickListener, OnEditorActionListener {

  public interface LoginListener {
    void onLoginFormReturned(String username, String password);
  }

  private static final String USERNAME_KEY = "key_username";
  private static final String PASSWORD_KEY = "key_password";
  private LoginListener loginListener;
  private EditText usernameEdit;
  private EditText passwordEdit;

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    setCancelable(false);
    if(activity instanceof LoginListener) {
      loginListener = (LoginListener) activity;
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_dialog_login, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    view.findViewById(R.id.login_button).setOnClickListener(this);
    view.findViewById(R.id.cancel_button).setOnClickListener(this);

    usernameEdit = (EditText) view.findViewById(R.id.login_username);
    passwordEdit = (EditText) view.findViewById(R.id.login_password);
    passwordEdit.setOnEditorActionListener(this);

    if(savedInstanceState != null) {
      usernameEdit.setText(savedInstanceState.getString(USERNAME_KEY));
      passwordEdit.setText(savedInstanceState.getString(PASSWORD_KEY));
    }
  }

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    Dialog dialog = super.onCreateDialog(savedInstanceState);
    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    return dialog;
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    outState.putString(USERNAME_KEY, usernameEdit.getText().toString());
    outState.putString(PASSWORD_KEY, passwordEdit.getText().toString());
    super.onSaveInstanceState(outState);
  }

  @Override
  public void onClick(@NonNull View view) {
    switch (view.getId()) {

      case R.id.login_button:
        login();
        break;

      case R.id.cancel_button:
        dismiss();
        break;
    }
  }

  @Override
  public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
    if (actionId == EditorInfo.IME_ACTION_DONE) {
      login();
      return true;
    }
    return false;
  }

  private void login() {

    if (loginListener != null) {
      String password = passwordEdit.getText().toString();
      String username = usernameEdit.getText().toString();
      loginListener.onLoginFormReturned(username, password);
    }
    dismiss();
  }

}
