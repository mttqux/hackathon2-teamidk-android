package com.mttnow.idk.shop.permissions;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

//package wrapper around the compat methods to allow for mocking
class PermissionCompat {

  public int checkSelfPermission(Context context, String permission) {
    return ContextCompat.checkSelfPermission(context, permission);
  }

  public boolean shouldShowRequestPermissionRationale(Activity activity, String permission) {
    return ActivityCompat.shouldShowRequestPermissionRationale(activity, permission);
  }

  public void requestPermissions(Activity activity, String[] permissions, int code) {
    ActivityCompat.requestPermissions(activity, permissions, code);
  }
}
