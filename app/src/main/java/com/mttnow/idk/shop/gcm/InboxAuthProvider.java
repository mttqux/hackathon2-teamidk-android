package com.mttnow.idk.shop.gcm;

import android.content.Context;
import android.widget.Toast;

import com.mttnow.android.messageinbox.InboxOperations;
import com.mttnow.android.messageinbox.rest.callbacks.InboxOperationsCallback;
import com.mttnow.identity.auth.client.Authentication;
import com.mttnow.identity.auth.client.AuthenticationProvider;
import com.mttnow.identity.auth.client.exceptions.IdentityAuthenticationException;

/**
 * Class that updates engage automatically when the user details change
 */
public class InboxAuthProvider implements AuthenticationProvider {

  private Context context;
  private AuthenticationProvider authenticationProvider;
  private InboxOperations inboxOperations;

  public InboxAuthProvider(Context context, AuthenticationProvider authenticationProvider, InboxOperations inboxOperations) {
    this.context = context.getApplicationContext();
    this.authenticationProvider = authenticationProvider;
    this.inboxOperations = inboxOperations;
  }

  @Override
  public Authentication provideAuthentication() throws IdentityAuthenticationException {
    return authenticationProvider.provideAuthentication();
  }

  @Override
  public void saveAuthentication(Authentication authentication) throws IdentityAuthenticationException {
    inboxOperations.registerGCMInbox(authentication.getUserUuid(), context, registerInboxCallback);
    authenticationProvider.saveAuthentication(authentication);
  }

  @Override
  public void clearAuthentication() throws IdentityAuthenticationException {
    if (provideAuthentication() != null) {
      inboxOperations.unregisterGCMInbox(provideAuthentication().getUserUuid(), unRegisterInboxCallback);
    }
    authenticationProvider.clearAuthentication();
  }

  private InboxOperationsCallback registerInboxCallback = new InboxOperationsCallback() {
    @Override
    public void onSuccess() {
      Toast.makeText(context, "Inbox registered for user", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailure() {
      Toast.makeText(context, "Inbox register failed for user", Toast.LENGTH_LONG).show();
    }
  };

  private InboxOperationsCallback unRegisterInboxCallback = new InboxOperationsCallback() {
    @Override
    public void onSuccess() {
      Toast.makeText(context, "Inbox unregistered for user", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailure() {
      Toast.makeText(context, "Inbox unregister failed for user", Toast.LENGTH_LONG).show();
    }
  };
}
