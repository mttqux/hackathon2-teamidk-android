package com.mttnow.idk.shop;

import android.Manifest;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.mttnow.android.journeywidget.ConciergeLive;
import com.mttnow.android.journeywidget.home.ConciergeLiveFragment;
import com.mttnow.idk.shop.permissions.DefaultPermissionRationale;
import com.mttnow.idk.shop.permissions.PermissionRationale;
import com.mttnow.idk.shop.permissions.PermissionsHelper;

/**
 * Server config are managed here.
 * the steps for handling the config and login
 * changed are as follows
 */
public class MainActivity extends AppCompatActivity {

  private static final String CONTEXT_FRAGMENT_TAG = "context_fragment";
  private final String[] PERMISSIONS = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
  private Toolbar toolbar;
  private PermissionsHelper permissionsHelper;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    setContentView(R.layout.activity_home);

    //find views
    toolbar = (Toolbar) findViewById(R.id.toolbar);

    //toolbar
    ViewCompat.setElevation(toolbar, 0);
    setSupportActionBar(toolbar);

    //permissions
    PermissionRationale permissionRationale = new DefaultPermissionRationale(this, getString(
        R.string.permission_rationale_title), PERMISSIONS, getResources().getStringArray(
        R.array.permission_rationals_home));
    permissionsHelper = new PermissionsHelper.Builder(this).permissions(PERMISSIONS)
        .rationale(permissionRationale)
        .savedSate(savedInstanceState)
        .build();
    permissionsHelper.requestPermissions();

    addJourneyContextFragment();

  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    permissionsHelper.onSaveInstanceState(outState);
  }


//  @Override
//  public boolean onOptionsItemSelected(MenuItem item) {
//    if (item.getItemId() == R.id.main_menu_refresh) {
//      ConciergeLive.component()
//          .journeyManager()
//          .emptyCache();
//      loadMessages();
//      return true;
//    }
//    return super.onOptionsItemSelected(item);
//  }


  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    permissionsHelper.permissionsResult(requestCode, permissions, grantResults);
  }

  private void clearCache() {
    ConciergeLive.component()
        .storage()
        .clear();
    ConciergeLive.component()
        .journeyManager()
        .emptyCache();
    ConciergeLive.component()
        .airportConfigFetcher()
        .emptyCached();
    ConciergeLive.component()
        .geofenceManager()
        .clearTriggeredFences();
  }

  //Removes and resets the context fragment after
  //reloading the config
  private void addJourneyContextFragment() {
    //remove old fragment
    FragmentManager fragmentManager = getSupportFragmentManager();

    //add new fragment that uses the new config
    if (fragmentManager.findFragmentByTag(CONTEXT_FRAGMENT_TAG) == null) {
      getSupportFragmentManager().beginTransaction()
          .add(R.id.activity_main_container, new ConciergeLiveFragment(), CONTEXT_FRAGMENT_TAG)
          .commit();
    }
    //get the messages
    loadMessages();
  }

  //removes the fragment, the fragment must be recreated and attached
  // using add addJourneyContextFragment();
  private void removeOldFragment() {
    FragmentManager fragmentManager = getSupportFragmentManager();
    Fragment fragment = fragmentManager.findFragmentByTag(CONTEXT_FRAGMENT_TAG);
    if (fragment != null) {
      fragmentManager.beginTransaction()
          .remove(fragment)
          .commit();
      fragmentManager.executePendingTransactions();
    }
  }

  //Load the messages if needed
  private void loadMessages() {
    ConciergeLive.instance()
        .updateMessages();
  }
}