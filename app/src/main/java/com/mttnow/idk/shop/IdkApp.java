package com.mttnow.idk.shop;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.mttnow.android.analytics.AnalyticsService;
import com.mttnow.android.analytics.AnalyticsServiceImpl;
import com.mttnow.android.journeywidget.CLHost;
import com.mttnow.android.journeywidget.ConciergeLive;
import com.mttnow.android.journeywidget.ConciergeLiveConfiguration;
import com.mttnow.android.journeywidget.home.CLMapClickListener;
import com.mttnow.android.journeywidget.home.JourneySyncResult;
import com.mttnow.android.journeywidget.home.JourneyUpdateListener;
import com.mttnow.android.messageinbox.InboxOperations;
import com.mttnow.android.messageinbox.InboxTemplate;
import com.mttnow.identity.auth.client.Authentication;
import com.mttnow.identity.auth.client.Credentials;
import com.mttnow.identity.auth.client.impl.DefaultIdentityAuthClient;
import com.mttnow.idk.shop.gcm.InboxAuthProvider;
import com.mttnow.idk.shop.login.SharedPreferencesAuthenticationProvider;
import com.mttnow.idk.shop.login.SharedPreferencesCredentialsProvider;
import com.mttnow.idk.shop.shop.BoardingPassActivity;
import com.mttnow.idk.shop.shop.ShopActivity;
import com.mttnow.journeycontext.client.model.Geofence;
import com.mttnow.journeycontext.client.model.Message;

import org.joda.time.DateTime;

public class IdkApp extends Application implements JourneyUpdateListener, CLHost, CLMapClickListener {

  public static final String idsEndpoint = "http://private-4159b-capitaids.apiary-mock.com";
  public static final String journeyContextEndpoint = "http://private-a22b9-idkhackathon2.apiary-mock.com";
  public static final String airportQueuesEndpoint = "http://private-4c06b2-idkhackathon3.apiary-mock.com";
  public static final String engageEndpoint = "http://concierge-devs1-bs-product.productdev.build.mttnow.com/mcs";

  private DefaultIdentityAuthClient identityAuthClient;

  @Override
  public void onCreate() {
    super.onCreate();

    initContextWidget();
  }

  public void initContextWidget() {


    com.mttnow.android.messageinbox.Config.Builder b = new com.mttnow.android.messageinbox.Config.Builder();
    b.endPoint(engageEndpoint);
    b.appName(getString(com.mttnow.android.messageinbox.R.string.message_inbox_application_name));
    b.senderId(getString(com.mttnow.android.messageinbox.R.string.message_inbox_gcm_sender_id));
    b.logging(com.mttnow.android.messageinbox.BuildConfig.DEBUG);

    InboxOperations inboxOperations = new InboxTemplate(this, b.build());

    Authentication authentication = new Authentication();
    authentication.setCreatedAt(DateTime.now());
    authentication.setExpiresAt(DateTime.now()
                                    .plusDays(2));
    authentication.setRefreshToken("refresh_tokkkenenene");
    authentication.setToken("refresh_tokkkenenene222");
    authentication.setUserUuid("userrrrrrrr");

    SharedPreferencesAuthenticationProvider authenticationProvider = new SharedPreferencesAuthenticationProvider(this);
    authenticationProvider.saveAuthentication(authentication);
    InboxAuthProvider inboxAuthProvider = new InboxAuthProvider(this, authenticationProvider, inboxOperations);

    SharedPreferencesCredentialsProvider credentialsProvider = new SharedPreferencesCredentialsProvider(this);
    credentialsProvider.saveCredentials(new Credentials("username1", "pasword1"));

    identityAuthClient = new DefaultIdentityAuthClient(idsEndpoint);
    identityAuthClient.setAuthenticationProvider(inboxAuthProvider);
    identityAuthClient.setCredentialsProvider(credentialsProvider);

    AnalyticsService analyticsService = new AnalyticsServiceImpl(this.getApplicationContext());

    ConciergeLive.initialize(new ConciergeLiveConfiguration.Builder().context(getApplicationContext())
                                 .identityAuthClient(identityAuthClient)
                                 .jcsEndpoint(journeyContextEndpoint)
                                 .aisEndpoint(airportQueuesEndpoint)
                                 .clListener(this)
                                 .conciergeLiveThemeId(R.style.ConciergeLiveTheme)
                                 .analyticsService(analyticsService)
                                 .clMapClickListener(this)
                                 .build());

    ConciergeLive.instance()
        .registerJourneyUpdateListener(this);
  }

  public DefaultIdentityAuthClient getIdentityAuthClient() {
    return identityAuthClient;
  }

  @Override
  public void onUpdateFinished(@NonNull JourneySyncResult updateResult) {
    if (!updateResult.isSuccess()) {
      if (updateResult.error != null) {
        updateResult.error.printStackTrace();
      }
      Toast toast = new Toast(this);
      toast.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);
      toast.setView(LayoutInflater.from(IdkApp.this)
                        .inflate(R.layout.offline_warning, null));
      toast.setDuration(Toast.LENGTH_LONG);
      toast.show();
    }
  }

  @Override
  public void onCheckInClicked(@NonNull Message message, @NonNull Context context) {
    Toast.makeText(context, "Check-in clicked " + message.getLegIdentifier()
        .toString(), Toast.LENGTH_LONG)
        .show();
  }

  @Override
  public void onBoardingPassClick(@NonNull Message message, @NonNull Context context) {
    if (message.getMetadata() != null && message.getMetadataEntry("is_shop") != null) {
      Intent intent = new Intent(this, ShopActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      startActivity(intent);
    } else {
      Intent intent = new Intent(this, BoardingPassActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      startActivity(intent);
    }
  }

  @Override
  public void onCancelledFlightClick(@NonNull Message message, @NonNull Context context) {
    Toast.makeText(context, "Cancelled flight clicked " + message.getLegIdentifier()
        .toString(), Toast.LENGTH_LONG)
        .show();
  }

  @Override
  public PendingIntent provideIntent(Context context, Geofence modelGeofence) {
    return PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), 0);
  }

  @Override
  public void onMapClicked(@NonNull Message message, @NonNull View view, @NonNull Context context) {
    Toast.makeText(context, "Static map button clicked: " + message.getCode(), Toast.LENGTH_LONG)
        .show();
  }

}
