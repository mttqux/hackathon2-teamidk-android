package com.mttnow.idk.shop.gcm;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.NotificationCompat;

import com.mttnow.android.messageinbox.gcm.delivery.InboxGcmListenerService;
import com.mttnow.idk.shop.MainActivity;
import com.mttnow.idk.shop.R;

public class DemoGcmService extends InboxGcmListenerService {

  public static final String NOTIFICATION_ID = "notificationId";
  public static final String NOTIFICATION_TEXT = "text";
  public static int notificationId = 1;

  @Override
  public void onMessageReceived(Bundle bundle) {

    notificationId++;
    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
    notificationManager.notify(notificationId, this.prepareNotification(bundle));
  }

  protected Notification prepareNotification(Bundle bundle) {
    String contentText = bundle.getString("text");
    PendingIntent contentIntent = this.preparePendingIntent(bundle);

    int color = ContextCompat.getColor(getApplicationContext(), R.color.color_primary);

    return new NotificationCompat.Builder(this).setSmallIcon(R.drawable.ic_notification)
      .setContentTitle(getText(R.string.app_name))
      .setColor(color)
      .setStyle((new NotificationCompat.BigTextStyle()).bigText(contentText))
      .setDefaults(NotificationCompat.DEFAULT_LIGHTS| NotificationCompat.DEFAULT_VIBRATE)
      .setContentText(contentText)
      .setContentIntent(contentIntent)
      .setAutoCancel(true)
      .build();
  }

  protected PendingIntent preparePendingIntent(Bundle bundle) {
    Intent intent = new Intent(this, MainActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
    intent.putExtra(NOTIFICATION_ID, notificationId);
    intent.putExtra(NOTIFICATION_TEXT, bundle.getString(NOTIFICATION_TEXT));
    return PendingIntent.getActivity(this, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
  }
}
