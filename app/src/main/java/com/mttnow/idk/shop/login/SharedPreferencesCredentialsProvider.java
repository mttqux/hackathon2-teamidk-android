package com.mttnow.idk.shop.login;

import android.content.Context;

import com.mttnow.identity.auth.client.Credentials;
import com.mttnow.identity.auth.client.CredentialsProvider;
import com.mttnow.identity.auth.client.exceptions.IdentityAuthenticationException;

public class SharedPreferencesCredentialsProvider implements CredentialsProvider {

  private static final String USERNAME_KEY = "USERNAME";
  private static final String PASSWORD_KEY = "PASSWORD";

  private SecuredPreferences securePrefs;

  public SharedPreferencesCredentialsProvider(Context context) {
    securePrefs = new SecuredPreferences(context);
  }

  @Override
  public Credentials provideCredentials() throws IdentityAuthenticationException {
    String username = securePrefs.getString(USERNAME_KEY, null);
    String password = securePrefs.getString(PASSWORD_KEY, null);

    if(username == null || password == null) {
      return null;
    }
    return new Credentials(username, password);
  }

  @Override
  public void saveCredentials(Credentials credentials) throws IdentityAuthenticationException {
    String username = credentials.getUsername();
    String password = credentials.getPassword();

    if (username != null && password != null) {
      SecuredPreferences.Editor editor = securePrefs.edit();
      editor.putString(USERNAME_KEY, credentials.getUsername());
      editor.putString(PASSWORD_KEY, credentials.getPassword());
      editor.apply();
    }
  }

  @Override
  public void clearCredentials() throws IdentityAuthenticationException {
    SecuredPreferences.Editor editor = securePrefs.edit();
    editor.remove(USERNAME_KEY);
    editor.remove(PASSWORD_KEY);
    editor.apply();
  }
}