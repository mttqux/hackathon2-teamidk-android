package com.mttnow.idk.shop.permissions;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages a set of permissions in a group. This will check and ask only for missing permissions.
 * It also provides a hook for showing a rational for the denied permission when you should show the rational.
 */
public class PermissionsHelper {

  private static final String IS_RUNNING_KEY = "pm_is_running";
  private static int requestCodes = 1;
  private final Activity activity;
  private final PermissionListener permissionListener;
  private final int code;
  private final String[] permissions;
  private final PermissionRationale permissionRationale;
  private final PermissionCompat permissionCompat;
  private boolean isRunning;

  public static class Builder {

    private Activity activity;
    private String[] permissions;
    private Bundle savedState;
    private PermissionListener permissionListener;
    private PermissionRationale permissionRationale;

    public Builder(@NonNull Activity activity) {
      this.activity = activity;
    }

    public Builder permissions(@NonNull String... permissions) {
      this.permissions = permissions;
      return this;
    }

    public Builder savedSate(@Nullable Bundle savedState) {
      this.savedState = savedState;
      return this;
    }

    public Builder listener(@Nullable PermissionListener permissionListener) {
      this.permissionListener = permissionListener;
      return this;
    }

    public Builder rationale(@Nullable PermissionRationale permissionRationale) {
      this.permissionRationale = permissionRationale;
      return this;
    }

    public PermissionsHelper build() {

      if (activity == null || permissions == null || permissions.length <= 0) {
        throw new IllegalArgumentException("Missing either activity or permissions for builder");
      }

      return new PermissionsHelper(null, activity, savedState, permissions,
        permissionListener, permissionRationale);
    }
  }

  //Used for mocking the permissions system calls in tests
  @VisibleForTesting
  PermissionsHelper(PermissionCompat permissionCompat, Activity activity, Bundle savedSate, String[] permissions, PermissionListener permissionListener, PermissionRationale permissionRationale) {
    this.activity = activity;
    this.permissionListener = permissionListener;
    requestCodes++;
    this.code = requestCodes;
    this.permissions = permissions == null ? new String[0] : permissions;
    this.permissionRationale = permissionRationale;

    if (this.permissions.length == 0) {
      throw new IllegalArgumentException("Permissions cannot be empty");
    }

    if (savedSate != null) {
      isRunning = savedSate.getBoolean(IS_RUNNING_KEY);
    }

    if (permissionCompat == null) {
      this.permissionCompat = new PermissionCompat();
    } else {
      this.permissionCompat = permissionCompat;
    }
  }

  /**
   * @return true if all the permissions have been granted
   */
  public boolean hasAllPermissions() {
    for (String permission : permissions) {
      if (permissionCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
        return false;
      }
    }
    return true;
  }

  /**
   * @return true if the permission has been granted
   */
  public boolean hasPermission(String permission) {
    return (permissionCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED);
  }

  /**
   * Need to be called in the {@link Activity#onRequestPermissionsResult(int, String[], int[])} the
   * activity callback will be called multiple times as the manager shows the permission requests and
   * rationals and request permissions
   *
   * @param requestCode  the request code returned
   * @param permissions  the permissions array returned
   * @param grantResults the grantResults array returned
   */
  public void permissionsResult(int requestCode, String[] permissions, int[] grantResults) {

    if (requestCode == code) {

      for (int i = 0; i < permissions.length; i++) {
        String permission = permissions[i];
        int grantResult = grantResults[i];

        if (grantResult == PackageManager.PERMISSION_DENIED && permissionCompat.shouldShowRequestPermissionRationale(activity, permission)) {
          //show rationals for the current permission
          showRational();
          notifyListener();
          return;
        }
      }
      isRunning = false;
      notifyListener();
    }
  }

  /**
   * To be called from the {@link PermissionRationale#showRationale(PermissionsHelper, String[])}
   * to tell the manager that the rational has been shown
   *
   * @param requestAgain true to request the permission again, false to respect the users decision
   */
  public void onRationalShown(boolean requestAgain) {
    if (requestAgain) {
      internalRequestPermissions();
    } else {
      notifyListener();
    }
  }

  /**
   * Trigger the permissions request
   */
  public void requestPermissions() {
    if (isRunning) {
      return;
    }
    isRunning = true;
    internalRequestPermissions();
  }

  public void onSaveInstanceState(Bundle outState) {
    outState.putBoolean(IS_RUNNING_KEY, isRunning);
  }

  public int getCode() {
    return code;
  }

  private void notifyListener() {
    if (permissionListener != null) {
      permissionListener.onPermissionRequestFinished();
    }
  }

  private void internalRequestPermissions() {
    String[] permissionsToRequest = getRequestablePermissions();
    if (permissionsToRequest.length > 0) {
      permissionCompat.requestPermissions(activity, permissionsToRequest, code);
    }
  }

  private String[] getRationalPermissions() {
    List<String> filteredPermissions = new ArrayList<>(permissions.length);
    //remove any permissions we have
    for (String permission : permissions) {
      if (permissionCompat.shouldShowRequestPermissionRationale(activity, permission)) {
        filteredPermissions.add(permission);
      }
    }
    String[] permissionsToRequest = new String[filteredPermissions.size()];
    filteredPermissions.toArray(permissionsToRequest);
    return permissionsToRequest;
  }

  private String[] getRequestablePermissions() {
    List<String> filteredPermissions = new ArrayList<>(permissions.length);
    //remove any permissions we have
    for (String permission : permissions) {
      if (permissionCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_DENIED) {
        filteredPermissions.add(permission);
      }
    }
    String[] permissionsToRequest = new String[filteredPermissions.size()];
    filteredPermissions.toArray(permissionsToRequest);
    return permissionsToRequest;
  }

  private void showRational() {
    if (permissionRationale == null) {
      return;
    }

    String[] permissionsToRational = getRationalPermissions();
    permissionRationale.showRationale(this, permissionsToRational);
  }

}
