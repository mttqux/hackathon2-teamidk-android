package com.mttnow.idk.shop.permissions;

public interface PermissionRationale {

  /**
   * Called when a the permission rational should be shown. You need to call {@link PermissionsHelper#onRationalShown(boolean)}
   * when user has seen/dismissed the rational as in using an {@link android.app.AlertDialog AlertDialog}
   *
   * @param permissionsHelper the {@link PermissionsHelper} for this rational
   * @param permissions        the lost of permissions to show the rational for.
   *                           This may be a subset of the requested if some are being auto denied or granted
   */
  void showRationale(PermissionsHelper permissionsHelper, String[] permissions);
}
