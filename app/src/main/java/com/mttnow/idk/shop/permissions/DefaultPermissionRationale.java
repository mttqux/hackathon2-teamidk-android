package com.mttnow.idk.shop.permissions;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.util.ArrayMap;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

/**
 * Default rationale shower that can be used with multiple permissions <-> rational pairs.
 * You can override {@link #showRationaleDialog(PermissionsHelper, String, String)} to
 * changed how the rationale is displayed
 */
public class DefaultPermissionRationale implements PermissionRationale {

  private final Context context;
  private final String title;
  private ArrayMap<String, String> permissionsMap = new ArrayMap<>();

  public DefaultPermissionRationale(Context context, String title, String permissions, String rationale) {
    this(context, title, new String[]{permissions}, new String[]{rationale});
  }

  public DefaultPermissionRationale(Context context, String title, String[] permissions, String[] rationales) {
    this.context = context;
    this.title = title;
    if (permissions.length != rationales.length) {
      throw new IllegalArgumentException("Each permission needs a matching rational");
    }
    for (int i = 0; i < permissions.length; i++) {
      permissionsMap.put(permissions[i], rationales[i]);
    }
  }

  public Context getContext() {
    return context;
  }

  @Override
  public void showRationale(final PermissionsHelper permissionsHelper, String[] permissions) {
    StringBuilder stringBuffer = new StringBuilder();
    //find the rationals for the denied permissions
    for (int i = 0; i < permissions.length; i++) {
      String permission = permissions[i];
      String rationale = permissionsMap.get(permission);

      if (!TextUtils.isEmpty(rationale)) {
        stringBuffer.append(rationale);
        if (i != permissions.length - 1) {
          stringBuffer.append("\n\n");
        }
      }
    }
    showRationaleDialog(permissionsHelper, title, stringBuffer.toString());
  }

  /**
   * Override to show the change how the rationale is shown
   */
  protected void showRationaleDialog(final PermissionsHelper permissionsHelper, String title, String concatenatedRationales) {
    new AlertDialog.Builder(getContext())
      .setTitle(title)
      .setMessage(concatenatedRationales)
      .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          dialog.dismiss();
          permissionsHelper.onRationalShown(true);
        }
      })
      .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          dialog.dismiss();
          permissionsHelper.onRationalShown(false);
        }
      })
      .show();
  }

}
