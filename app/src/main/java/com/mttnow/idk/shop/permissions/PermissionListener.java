package com.mttnow.idk.shop.permissions;

public interface PermissionListener {

  /**
   * Called when the permission manager has asked processed all the permissions
   */
  void onPermissionRequestFinished();
}
