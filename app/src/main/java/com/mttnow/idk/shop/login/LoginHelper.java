package com.mttnow.idk.shop.login;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.mttnow.identity.auth.client.AuthenticationResult;
import com.mttnow.identity.auth.client.impl.DefaultIdentityAuthClient;

/**
 * Manages the login state for the app and manages the login fragment
 */
public class LoginHelper implements LoginDialogFragment.LoginListener {

  private LoginTask loginTask;
  private static final String LOGIN_FRAGMENT_TAG = "login_fragment";

  public interface LoginCallback {
    void onLoginChanged(boolean success);
  }

  private DefaultIdentityAuthClient identityAuthClient;

  private AppCompatActivity activity;
  private LoginCallback callback;

  public LoginHelper(DefaultIdentityAuthClient identityAuthClient, AppCompatActivity activity, LoginCallback callback) {
    this.identityAuthClient = identityAuthClient;
    this.activity = activity;
    this.callback = callback;
  }

  public void performLogin() {
    if (isLoggedIn()) {
      callback.onLoginChanged(true);
    } else if (activity.getSupportFragmentManager().findFragmentByTag(LOGIN_FRAGMENT_TAG) == null) {
      //login fragment is already shown
      LoginDialogFragment loginDialogFragment = new LoginDialogFragment();
      loginDialogFragment.show(activity.getSupportFragmentManager(), LOGIN_FRAGMENT_TAG);
    }
  }

  public void logout() {
    identityAuthClient.logout();
    callback.onLoginChanged(false);
    performLogin();
  }

  public void updateAuthClient(DefaultIdentityAuthClient newAuthClient) {
    identityAuthClient.logout();
    callback.onLoginChanged(false);
    this.identityAuthClient = newAuthClient;
    performLogin();
  }

  public boolean isLoggedIn() {
    return identityAuthClient.getCredentialsProvider().provideCredentials() != null;
  }

  @Override
  public void onLoginFormReturned(String username, String password) {
    executeLogin(username, password);
  }

  public void destroy() {
    if (loginTask != null) {
      loginTask.cancel(true);
    }
  }

  private void executeLogin(String username, String password) {
    if (loginTask != null) {
      loginTask.cancel(true);
    }
    loginTask = new LoginTask(username, password);
    loginTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }

  //task that logs a user in and then resets the journeycontextfragment
  //when the login succeeds
  private class LoginTask extends AsyncTask<Void, Void, Pair<AuthenticationResult, Exception>> {

    private String username;
    private String password;

    private LoginTask(String username, String password) {
      this.username = username;
      this.password = password;
    }

    @Override
    protected Pair<AuthenticationResult, Exception> doInBackground(Void... params) {
      try {
        return Pair.create(identityAuthClient.login(username, password), null);
      } catch (Exception exception) {
        return Pair.create(null, exception);
      }
    }

    @Override
    protected void onPostExecute(@NonNull Pair<AuthenticationResult, Exception> result) {
      AlertDialog.Builder builder = new AlertDialog.Builder(activity);
      builder.setTitle("Concierge Live Demo");

      AuthenticationResult authenticationResult = result.first;
      Exception error = result.second;

      if (authenticationResult == null) {
        buildGeneralError(builder, error);
      } else if (authenticationResult.succeed()) {
        builder.setMessage("Login Successful for user: " + authenticationResult.getAuthentication().getUserUuid());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            callback.onLoginChanged(true);
          }
        });
      } else if (authenticationResult.hasError()) {
        builder.setMessage("Login failed due to " + authenticationResult.getError().getDefaultMessage());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            callback.onLoginChanged(false);
          }
        });
      } else {
        buildGeneralError(builder, null);
      }
      builder.setCancelable(false);
      builder.show();
    }

    private void buildGeneralError(AlertDialog.Builder builder, Exception error) {
      builder.setMessage("Login failed due to " + (error == null ? "" : error.getMessage()));
      builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          dialog.dismiss();
          performLogin();
          callback.onLoginChanged(false);
        }
      });
    }
  }



}
